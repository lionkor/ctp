#pragma once

#include <pthread.h>
#include <stdatomic.h>

/* function signature for a function to be submitted as a job
 * to the thread pool */
typedef void (*ctp_work_fn_t)(void*);

/* all possible kinds of ctp errors */
enum ctp_error {
    CTP_SUCCESS,
    CTP_ERR_ALLOC,
    CTP_ERR_PTHREAD,
    CTP_BUSY
};

typedef enum ctp_error ctp_error;

#ifdef BUILDING_CTP
/* holds all data for the thread pool - do not access manually */
struct ctp_pool {
    pthread_t* threads;
    pthread_attr_t* attrs;
    ctp_work_fn_t* jobs;
    void** args;
    pthread_mutex_t* jobs_mutexes;
    pthread_mutexattr_t* jobs_mutexes_attrs;
    pthread_cond_t* condition_vars;
    pthread_condattr_t* condition_vars_attrs;

    /* number of threads running in the pool */
    size_t n_threads;
    /* whether to shut down */
    atomic_bool shutdown;
};
typedef struct ctp_pool ctp_pool;
#else
typedef void ctp_pool;
#endif

/* allocates a new thread pool.
 * returns NULL on error and sets *ep to the error, if not NULL.
 * the return value is the pool, which must be freed via ctp_destroy(). */
ctp_pool* ctp_new(size_t n_threads, ctp_error* ep);
/* stops and deallocates a pool.
 * waits for all work to be done before shutting down a thread. */
void ctp_destroy(ctp_pool* pool);
/* adds the job as a job to the pool, with `arg` as call arguments.
 * a work thread from the pool will then pick up the job and call it like:
 *  job(arg); */
ctp_error ctp_add_job(ctp_pool* pool, ctp_work_fn_t job, void* arg);
