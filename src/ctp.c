#include <ctp.h>
#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

struct ctp_main_args {
    struct ctp_pool* pool;
    size_t index;
};
typedef struct ctp_main_args ctp_main_args;

static void* ctp_main(void* args_ptr) {
    ctp_main_args* args = args_ptr;
    ctp_pool* pool = args->pool;
    size_t index = args->index;
    struct timespec wait;

    wait.tv_sec = 0;
    wait.tv_nsec = 100000000lu; /* 100 ms */

    while (true) {
        ctp_work_fn_t fn = NULL;
        void* arg = NULL;

        pthread_mutex_lock(&pool->jobs_mutexes[index]);
        pthread_cond_timedwait(&pool->condition_vars[index], &pool->jobs_mutexes[index], &wait);
        if (pool->jobs[index]) {
            fn = pool->jobs[index];
            arg = pool->args[index];
            pool->args[index] = NULL;
        }
        pthread_mutex_unlock(&pool->jobs_mutexes[index]);
        if (fn) {
            fn(arg);
            pthread_mutex_lock(&pool->jobs_mutexes[index]);
            pool->jobs[index] = NULL;
            pthread_mutex_unlock(&pool->jobs_mutexes[index]);
        } else {
            if (atomic_load(&pool->shutdown)) {
                break;
            }
            sched_yield();
        }
    }

    free(args);
    return NULL;
}

/* given a large enough pre-allocated buffer of memory, `raw`, allocates `n`
 * elements of size `size` and modifies `offset` to a new part of `raw`.
 * this function can be used inplace of `calloc` with a pre-allocated block of
 * memory, in order to reduce allocations and make deallocation easier. */
static void* ctp_offset_alloc(size_t n, size_t size, void* raw, size_t* offset) {
    uint8_t* bytes = raw;
    void* res = bytes + *offset;
    *offset += size * n;
    return res;
}

/* brainless allocation of each array with error handling.
 * on failure, returns CTP_ERR_ALLOC.
 * in this case, ctp_free_pool_memory must be called. */
static ctp_error ctp_initialize_pool_memory(ctp_pool* pool) {
    size_t offset = 0;
    /* allocate enough memory for all members */
    void* arena = calloc(pool->n_threads,
        sizeof(pthread_t)
            + sizeof(pthread_attr_t)
            + sizeof(ctp_work_fn_t)
            + sizeof(void*)
            + sizeof(pthread_mutex_t)
            + sizeof(pthread_mutexattr_t)
            + sizeof(pthread_cond_t)
            + sizeof(pthread_condattr_t));
    if (!arena) {
        perror("calloc");
        return CTP_ERR_ALLOC;
    }
    /* use offset "allocation" to grab memory from the previously allocated arena */
    pool->threads = ctp_offset_alloc(pool->n_threads, sizeof(pthread_t), arena, &offset);
    pool->attrs = ctp_offset_alloc(pool->n_threads, sizeof(pthread_attr_t), arena, &offset);
    pool->jobs = ctp_offset_alloc(pool->n_threads, sizeof(ctp_work_fn_t), arena, &offset);
    pool->args = ctp_offset_alloc(pool->n_threads, sizeof(void*), arena, &offset);
    pool->jobs_mutexes = ctp_offset_alloc(pool->n_threads, sizeof(pthread_mutex_t), arena, &offset);
    pool->jobs_mutexes_attrs = ctp_offset_alloc(pool->n_threads, sizeof(pthread_mutexattr_t), arena, &offset);
    pool->condition_vars = ctp_offset_alloc(pool->n_threads, sizeof(pthread_cond_t), arena, &offset);
    pool->condition_vars_attrs = ctp_offset_alloc(pool->n_threads, sizeof(pthread_condattr_t), arena, &offset);

    return CTP_SUCCESS;
}

static void ctp_free_pool_memory(ctp_pool* pool) {
    free(pool->threads);
}

static ctp_error ignore_error;

ctp_pool* ctp_new(size_t n_threads, ctp_error* ep) {
    size_t i = 0;
    ctp_pool* pool = NULL;
    ctp_main_args* args = NULL;

    if (!ep) {
        ep = &ignore_error;
    }
    *ep = CTP_SUCCESS;

    pool = calloc(1, sizeof(ctp_pool));
    if (!pool) {
        perror("calloc");
        *ep = CTP_ERR_ALLOC;
        goto ret;
    }
    pool->n_threads = n_threads;

    *ep = ctp_initialize_pool_memory(pool);
    if (*ep != CTP_SUCCESS) {
        goto ret;
    }

    atomic_store(&pool->shutdown, false);
    for (i = 0; i < pool->n_threads; ++i) {
        int res;
        res = pthread_condattr_init(&pool->condition_vars_attrs[i]);
        if (res != 0) {
            perror("pthread_condattr_init");
            *ep = CTP_ERR_PTHREAD;
            goto ret;
        }
        res = pthread_cond_init(&pool->condition_vars[i], &pool->condition_vars_attrs[i]);
        if (res != 0) {
            perror("pthread_cond_init");
            *ep = CTP_ERR_PTHREAD;
            goto ret;
        }
        res = pthread_mutexattr_init(&pool->jobs_mutexes_attrs[i]);
        if (res != 0) {
            perror("pthread_mutexattr_init");
            *ep = CTP_ERR_PTHREAD;
            goto ret;
        }
        res = pthread_mutex_init(&pool->jobs_mutexes[i], &pool->jobs_mutexes_attrs[i]);
        if (res != 0) {
            perror("pthread_mutex_init");
            *ep = CTP_ERR_PTHREAD;
            goto ret;
        }
        res = pthread_attr_init(&pool->attrs[i]);
        if (res != 0) {
            perror("pthread_attr_init");
            *ep = CTP_ERR_PTHREAD;
            goto ret;
        }
        args = calloc(1, sizeof(ctp_main_args));
        if (!args) {
            perror("calloc");
            *ep = CTP_ERR_ALLOC;
            goto ret;
        }
        args->index = i;
        args->pool = pool;
        res = pthread_create(&pool->threads[i], &pool->attrs[i], ctp_main, args);
        if (res != 0) {
            perror("pthread_create");
            *ep = CTP_ERR_PTHREAD;
            return NULL;
        }
    }
ret:
    if (*ep != CTP_SUCCESS) {
        ctp_free_pool_memory(pool);
        free(pool);
        free(args);
        return NULL;
    } else {
        return pool;
    }
}

void ctp_destroy(ctp_pool* pool) {
    if (pool) {
        size_t i = 0;
        atomic_store(&pool->shutdown, true);
        for (i = 0; i < pool->n_threads; ++i) {
            int detachstate = 0;
            pthread_attr_getdetachstate(&pool->attrs[i], &detachstate);
            if (detachstate == PTHREAD_CREATE_JOINABLE) {
                pthread_join(pool->threads[i], NULL);
            }
            pthread_mutexattr_destroy(&pool->jobs_mutexes_attrs[i]);
            pthread_mutex_destroy(&pool->jobs_mutexes[i]);
            pthread_cond_destroy(&pool->condition_vars[i]);
            pthread_condattr_destroy(&pool->condition_vars_attrs[i]);
            pthread_attr_destroy(&pool->attrs[i]);
        }
        ctp_free_pool_memory(pool);
    }
    free(pool);
}

ctp_error ctp_add_job(ctp_pool* pool, ctp_work_fn_t job, void* arg) {
    ctp_error err = CTP_SUCCESS;
    bool found = false;
    static size_t last_i = 0;
    size_t i = (last_i) % pool->n_threads;
    while (!found) {
        if (!pool->jobs[i]) {
            pthread_mutex_lock(&pool->jobs_mutexes[i]);
            if (!pool->jobs[i]) {
                pool->jobs[i] = job;
                pool->args[i] = arg;
                found = true;
                last_i = 0;
                pthread_mutex_unlock(&pool->jobs_mutexes[i]);
                pthread_cond_signal(&pool->condition_vars[i]);
                break;
            }
            pthread_mutex_unlock(&pool->jobs_mutexes[i]);
        }
        ++i;
        i %= pool->n_threads;
    }
    last_i = i;
    if (!found) {
        err = CTP_BUSY;
    }
    return err;
}
