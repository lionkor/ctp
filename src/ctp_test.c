#include <string.h>
#define _BSD_SOURCE
#define _DEFAULT_SOURCE

#include <ctp.h>

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

static void work1(void* arg) {
    size_t size = 1024 * 1024;
    size_t i = 0;
    volatile size_t res = 0;
    (void)arg;
    for (i = 0; i < size; ++i) {
        res += i;
    }
}

int main(int argc, char** argv) {
    ctp_error err = CTP_SUCCESS;
    ctp_pool* pool = NULL;
    size_t i = 0;
    size_t jobs = 0;

    pool = ctp_new(100, &err);
    if (!pool) {
        return -1;
    }

    if (argc == 1) {
        jobs = 10000;
    } else {
        jobs = atoi(argv[1]);
    }

    for (i = 1; i < jobs; ++i) {
        do {
            err = ctp_add_job(pool, work1, (void*)i);
        } while (err != CTP_SUCCESS);
    }

    ctp_destroy(pool);

    return 0;
}
