# ctp

`ctp` is a C89 pthread-based thread pool implementation.

## Building

To build the library, run:

1. `meson setup -C bin`
2. `meson compile -C bin`

Now the library will be available in `bin/libctp.so`.

You may install as root using:
```sh
# meson install -C bin
```

## Usage


1. Instantiate a threadpool
```c
ctp_pool* pool;
ctp_error err;

pool = ctp_new(&err);

if (!pool) {
    /* inspect err if you want */
    exit(1);
}
```
2. Add work to the pool
```c
err = ctp_add_job(pool, my_work, my_arg);
if (err == CTP_BUSY) {
    /* TODO: no threads were free, retry, wait, or something else. */
}
```
your `my_work` may look like this, and `my_arg` will be passed to `arg`:
```c
static void my_work(void* arg) {
    /* use arg ...
     * do work ... */
}
```
3. Destroy the pool (this will wait for work to be completed)
```c
ctp_destroy(pool);
```
